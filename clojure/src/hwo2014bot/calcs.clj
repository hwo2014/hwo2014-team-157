(ns hwo2014bot.calcs (:require [hwo2014bot.racevariables :as rv] [hwo2014bot.models])
  (:import [hwo2014bot.models CarRouteCrossing] 
           ))
  
  (def branch-size (atom 2))
  
  (defn- piece-indexes-from-parts [part]
    (map #(:piece %) part))
  
  
  (defn- get-routes-with-piece-index [routes piece-index piece-distance]
    (let [piece-indexed-routes (map #(list (piece-indexes-from-parts (:piece-parts %)) %) routes)
          filtered
          (filter #(> (.indexOf (first %) piece-index) -1)
                  piece-indexed-routes)
          ]
      (map second filtered)
      ))
  
  (defn safe-reference-count [count-in ref-in]
    (rem (+ ref-in count-in) count-in))
  
  (defn safe-reference [list-in ref-in]
    (rem (+ ref-in (count list-in)) (count list-in)))
  
  (defn safe-entry [list-in ref-in]
    (nth list-in (safe-reference list-in ref-in)))
  
  (defn handle-define [data-in]
    ;(println "***In define... max-angle" (:angle  (last data-in)))
    
    (loop [i 0
           data []
           stage "zeros"]
      (if (< i (count data-in))
        (let [car-data (nth data-in i)]
          (cond 
            (= stage "zeros")
            (if (pos? (:velocity car-data))
              (do (println "first vel" (:velocity car-data))
                  (reset! rv/aadd-a (:velocity car-data))
                  (recur (inc i) [(:velocity car-data)] "second-speed")
                  )
              (recur (inc i) [] "zeros")
              )
            (= stage "second-speed")
            (do (println "second vel" (:velocity car-data))
                (println "decel = " (- 1.0 (/ (- (:velocity car-data) (first data)) (first data))))
                (reset! rv/decel-multiplier-a  (- 1.0 (/ (- (:velocity car-data) (first data)) (first data))))
                (recur (inc i) [] "regen")
                )                   
            (= stage "angles")
            (let [difference (Math/abs (if (empty? data) 0 (- (:angle car-data)  (last data))))]            
              
              (if (= 14 (count data))
                  (let [piece (first (:piece-parts (:starting-route car-data))) 
                        v (:velocity piece)                       
                        diff-norm (/ (* difference (Math/pow (:radius piece) 2)) (Math/pow 110 2))
                        new-fmax (* @rv/fmax-a (/ diff-norm 2.3) (/ v 7.1))]
                  (do
                    (println "difference : " difference)
                    (reset! rv/fmax-a new-fmax)
                    (println "new fmax-a" @rv/fmax-a) 
                  (recur (inc i) (conj data (:angle car-data)) "regen"))
                  )
                (if  (> (:angle car-data) 0)
                  (do 
                    (let [piece (first (:piece-parts (:starting-route car-data)))]
                    (println i "count" (count data) ":found angle v,angle,r, lane, piece:" (:velocity car-data)  "," (:angle car-data) "," (:radius piece) "," (:lane piece) "," (:piece piece) "difference" difference)
                    
                    (recur (inc i) (conj data (:angle car-data)) "angles")))
                  (recur (inc i) [] "angles"))
                
                ))
            
            (= stage "regen")
            true                        
            )
          )   
        )
      )
    
    )
  
  ;(defn get-starting-route [lanes piece-index piece-distance-in lane-start lane-end]
  
  (defn get-starting-route [lanes location]
    ;(println location)
    (loop [i 0
           lanes-to-check (if (nil? (:lane2 location)) [(:lane1 location) (:lane1 location)] [(:lane1 location) (:lane2 location)])
           piece-distance (:piece-distance location)]
      (let [routes (nth lanes (nth lanes-to-check i))
            full-route
            (nth (get-routes-with-piece-index routes (:piece location) (:piece-distance location)) i)
            pieces (:piece-parts full-route)
            remaining-pieces (into [] (drop-while #(not (= (:piece location) (:piece %))) pieces))
            current-piece (first remaining-pieces)
            effective-branch-size (* i @branch-size)
            
            remaining-current-piece
            (assoc current-piece :length (+ (- (:length current-piece) piece-distance) effective-branch-size)
                   :distance-into-turn (+ (:distance-into-turn current-piece) piece-distance)
                   :total-turn-length (- (:total-turn-length current-piece) piece-distance)
                   )
            new-pieces (assoc remaining-pieces 0 remaining-current-piece)
            ]
        (if (or (not (:switch-piece current-piece))
                (= i (- (count lanes-to-check) 1))
                (>= (:length current-piece) piece-distance))
          (do
            ;(println lanes-to-check)
            ;(println (:length current-piece) piece-distance)
            
            ;(println 
            ; (not (:switch-piece current-piece))
            ;   (= i (- (count lanes-to-check) 1))
            ;  (>= (:length current-piece) piece-distance))
            
            (assoc full-route :piece-parts
                   new-pieces))
          (recur (inc i) lanes-to-check (- piece-distance (:length current-piece))) 
          ))))
  
  (defn calculate-speed [lanes location-1 location-2]
    ;(println location-1)
    (let [route-1 
          (get-starting-route lanes location-1)
          
          route-1-piece (-> route-1 :piece-parts first)
          route-1-piece-no (:piece route-1-piece)
          route-1-piecepart (:piecepart route-1-piece)
          route-1-lane (:lane route-1-piece)
          route-1-no (:route route-1-piece)
          route-2
          (get-starting-route lanes location-2)
          route-2-piece (-> route-2 :piece-parts first)
          route-2-piece-no (:piece route-2-piece)          
          route-2-piecepart (:piecepart route-2-piece)
          route-2-lane (:lane route-2-piece)         
          route-2-no (:route route-2-piece)
          
          return-val
          
          (cond
            
            (and (= route-1-piece-no route-2-piece-no) (= route-1-lane route-2-lane));same piece and lane        
            (- (:piece-distance location-2) (:piece-distance location-1)) ;can just take the distance
            
            (= route-1-lane route-2-lane) ;same lane, different piece
            (+ (:length route-1-piece) (:piece-distance location-2));remaining on 1 and distance on 2
            
            (= route-1-piece-no route-2-piece-no) ;different branch same piece-index
            (+ (- (:piece-distance location-2) (:piece-distance location-1)))
            :else ;Oh no, we've crossed and skipped the remaining, shouldn't happen but return nil? TODO fix?
            (println "XXXXXX We shouldn't be here, can't calc speed!" location-1 location-2)              
            )
          _ (if (or (nil? return-val) (> return-val 30)) 
              
              (println "Somethings wrong!" route-1-no route-1-lane route-1-piecepart route-2-no route-2-lane route-2-piecepart route-1-piece route-2-piece
                       (and (= route-1-piecepart route-2-piecepart) (= route-1-lane route-2-lane))                       
                       (= route-1-lane route-2-lane) 
                       (= route-1-piecepart route-2-piecepart)
                       (:piece-distance location-2)                                 
                       )
              
              
              )
          ]
      (if (nil? return-val ) 5 return-val)
      ))
  
  (defn calculate-throttle-for-tick [v0 v1 v2 turbo-time car vtar]
    (let [decel (* v1 @rv/decel-multiplier-a)
          remaining (+ (- v2 v1) decel)
          ideal (/ remaining @rv/aadd-a)          
          angle (Math/abs (:angle car))
          previous-angle (Math/abs (:previous-angle car))
          
          starting-piece (first (:piece-parts (:starting-route car)))
          turn-in (:distance-into-turn starting-piece)
          turn-remaining (:total-turn-length starting-piece)
          ticks-remaining (if (pos? v0) (/ turn-remaining v0) 100)
          ]
      (cond
        
        (and (> angle previous-angle) (> angle 25) (< turn-in (* 12 v0)))
        (do (println "throttle limiting 25")
        0)
        
        (and (> angle previous-angle) (> angle 40) (< turn-in (* 17 v0)))
        (do (println "throttle limiting 40")
        0)

        (and (> angle previous-angle) (> angle 56) (< turn-in (* 22 v0)))
        (do (println "throttle limiting 56")
        0)
        
        (and (> angle previous-angle) (< turn-remaining (* 22 v0)) 
            (> (+ (* (- angle previous-angle) ticks-remaining) angle) 59))             
        (do (println "throttle limiting due to time remainining..")
            0)
        
        
        (and (< turbo-time 30) (> v0 vtar))
        0
 
        (< v1 0.1);Ramming speed
        1
        (> v0 (+ v1 0.20)) 0
        (> ideal 0.99) 1
        (< ideal 0.01) 0
        
        :else ideal
        )
      ))

        (comment
        (and (> angle previous-angle) (> angle 34) (> turn-remaining (* 13 v0)))
        (do (println "throttle limiting 40 not far to go")
        0)
                
        (and (> angle previous-angle) (> angle 39) (> turn-remaining (* 10 v0)))
        (do (println "throttle limiting 40 not far to go")
        0)
        
        (and (> angle previous-angle) (> angle 44) (> turn-remaining (* 8 v0)))
        (do (println "throttle limiting 45 not far to go")
        0)
                
        (and (> angle previous-angle) (> angle 49) (> turn-remaining (* 5 v0)))
        (do (println "throttle limiting 50 not far to go")
        0))
(defn consolidate-grouping[route-ticks]
  (reduce #(list (first %1) 
                 (second %1) 
                 (+ (nth %1 2) (nth %2 2))) 
          route-ticks)) 

  (defn map-ticks-to-route-times [ticks-in]
    (let [summary-list (map #(list (nth % 5) (nth % 3) (count (nth % 2))) ticks-in)
          grouped-summary (group-by first summary-list)
          ;_ (println grouped-summary)
          reduced (map consolidate-grouping (vals grouped-summary)) ]
      (loop [i 0
             return-vals []
             total-timing 0
             ]
        
        (if (< i (count reduced))
          (recur (inc i)
                 (conj return-vals (CarRouteCrossing. (first (nth reduced i)) (second (nth reduced i) )                                                   
                                                      (+ total-timing (nth (nth reduced i) 2)
                                                         )))
                 (+ total-timing (nth (nth reduced i) 2))                 
                 )
          return-vals
          )))
    
    ;(map (group-by #(nth % 5) ticks-in) )
    
    ;(map #(CarRouteCrossing. (nth % 5) (nth % 3) (count (nth % 2))) ticks-in)
    
    )

(defn reduce-crossing-times [crossings ratio]
  (map #(assoc % :timing (* ratio (:timing %))) crossings )
  )
  
  