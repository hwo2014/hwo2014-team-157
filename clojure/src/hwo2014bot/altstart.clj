(ns hwo2014bot.altstart
  (:require [hwo2014bot.core :as bot]
            [hwo2014bot.racevariables :as rv])
  (:gen-class))


   (defn -main[& [botname botkey msgType type track]]
     
     (if (= type "slow")
       (println (reset! rv/vmax-a 3.012)))
     (bot/manual-connect "prost.helloworldopen.com" "8091" botname botkey msgType track)
     )
