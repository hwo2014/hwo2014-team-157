(ns hwo2014bot.core
  (:require [clojure.data.json :as json]
            [hwo2014bot.msg :as msg]
            [hwo2014bot.mosquitoes :as moz]
            [hwo2014bot.intel :as intel]
            [hwo2014bot.calcs :as calcs]
            [hwo2014bot.racevariables :as rv])
  (:use [aleph.tcp :only [tcp-client]]
        [lamina.core :only [enqueue wait-for-result wait-for-message]]
        [gloss.core :only [string]])
  (:gen-class))

(def version 1.5)
(def log-mode false)
(def turbos-enabled true)
(def initialised false)

(def current-lanes-a (atom nil))
(def last-switch-sent (atom -10));game-tick
;(def previous-location-a (atom [0 0 0 0]))
(def current-throttle (atom 0))
(def last-throttle-sent (atom 0))
(def predicted-v (atom 0))
(def car-color (atom "red"))
(def turbos-available (atom 0))
(def last-turbo-time (atom -50))
(def in-define-mode (atom true))
(def define-data (atom []))




(defn- json->clj [string]
  (json/read-str string :key-fn keyword))

(defn send-message [channel message]
  (let [msg (json/write-str message)]
    (enqueue channel msg)))

(def filename (str "log_" (System/currentTimeMillis) ".json"))

(defn- json->clj [string]
  (do 
    (if log-mode
      (spit (str "in_" filename) (str string "\n\n") :append true))
    (json/read-str string :key-fn keyword)))

(defn send-message [channel message]
  (let [msg (json/write-str message)]
    (do
      (if log-mode
        (spit (str "out_"filename) (str msg "\n\n") :append true))
      ;(println "Message out " (System/currentTimeMillis) (msg/all-game-tick message))
      (enqueue channel msg))))


(defn read-message [channel]
  (json->clj
    (try
      (wait-for-message channel)
      (catch Exception e
             (println (str "ERROR: " (.getMessage e)))
             (System/exit 1)))))

(defn connect-client-channel [host port]
  (wait-for-result
    (tcp-client {:host host,
                 :port port,
                 :frame (string :utf-8 :delimiters ["\n"])})))

(defn get-switch-direction [lane-in lane-out]
  (if (> (- lane-in lane-out) 0)
    "Left"
    "Right"
    ))

(defn get-velocity-in-future [ticks future-tick]
  (loop [i 0
         index future-tick]
    (let [this-tick (nth ticks i)
          velocities (nth this-tick 2)]
      (if (> (count velocities) index)
        (nth velocities index)
        (recur (inc i) (- index (count velocities)))
        ))))

(defn get-racing-line-avoid-others [car cars-ahead]
  ;(if (> (count cars-ahead) 0) (println "**** OPPONENT DETECTED"))
  
  (let [other-route-crossings
        (loop [i 0
               route-crossings []]
          (if (< i (count cars-ahead))
            (let [other-line (moz/get-racing-line @current-lanes-a (:velocity (nth cars-ahead i)) (:location (nth cars-ahead i)) nil 2 route-crossings)]
              ;(do (println (:ticks 
              ;          other-line))
              (recur (inc i)
                     (calcs/map-ticks-to-route-times
                       (:ticks 
                         other-line))))
            route-crossings
            ))
        
        our-line (moz/get-racing-line
                   ;lanes            speed-in        location       next-lane  
                   @current-lanes-a (:velocity car)  (:location car) nil  
                   ;calc-depth                       existing cars     
                   ;(count (first @current-lanes-a)) other-route-crossings   
                   2 (calcs/reduce-crossing-times other-route-crossings 0.95)    
                   
                   )
        ]
    
    ;(println other-route-crossings)
    ;(println (calcs/map-ticks-to-route-times
    ;           (:ticks  our-line)))
    ;(println (:ticks  our-line))
    ;(println (first (:piece-parts (:starting-route car))))
    our-line
    ))

(defmulti handle-msg :msgType)

(defmethod handle-msg "gameInit" [msg]
  (println "gameInit received" msg)
  (if (not initialised)
    (do
      (moz/init-track (msg/game-init-track msg))
      (reset! current-lanes-a (moz/update-routes moz/track-lanes))
      (reset! intel/cars {})
      (def initialised true)
      ))
  (reset! last-switch-sent -10)
  (reset! last-throttle-sent -10)
  (reset! last-turbo-time -50)
  (reset! turbos-available 0)
  
  ;(future (Thread/sleep 20) (System/gc))
  {:msgType "ping" :data "ping"}
  )

(defmethod handle-msg "yourCar" [msg]
  (println "Am setting car color to: " msg/your-car-color msg)
  (reset! car-color (msg/your-car-color msg))
  
  {:msgType "ping" :data "ping"}
  ),

(defmethod handle-msg "turboAvailable" [msg]
  (reset! turbos-available 1)
  (println "***We have turbo " msg)
  ;(future (Thread/sleep 20) (System/gc))  
  {:msgType "ping" :data "ping"}
  )

(defmethod handle-msg "crash" [msg]
  (println "Someone crashed " (:color (:data msg)) " us " (= (:color (:data msg)) @car-color))
  (comment if (and (= (:color (:data msg)) @car-color ) @in-define-mode)
           (do
             (reset! in-define-mode false)
             (calcs/handle-define @define-data)
             (reset! current-lanes-a (moz/update-routes moz/track-lanes))
             ;(future (Thread/sleep 20) (System/gc))      
             ))
  
  {:msgType "ping" :data "ping"}
  )

(defmethod handle-msg "carPositions" [msg]
  (try
    
    (let [_ (intel/update msg @current-lanes-a)
          car (intel/get-car @car-color)
          game-tick (msg/all-game-tick msg)
          cars-ahead (intel/get-cars-ahead @current-lanes-a @car-color 2)]
      (if (-> game-tick nil?)
        {:msgType "throttle" :data 1}
        
        
        (let [
              velocity (:velocity car)
              ;angle (:angle car)
              ;start-lane (:lane1 (:location car))
              ;end-lane (:lane2 (:location car))
              ;piece-index (:piece (:location car))
              ;piece-distance-in (:piece-distance (:location car))
              fastest-line
              (get-racing-line-avoid-others car cars-ahead)
              
              ticks (:ticks fastest-line)
              ;test (println ticks)
              next-lane (:lane (second (:routes fastest-line)))
              this-route (first (:routes fastest-line))
              this-piece-part (first (:piece-parts (:starting-route car)))
              this-lane (:lane (first (:routes fastest-line)))
              speed-options (:speed-options this-piece-part)
              this-speed-option (nth speed-options next-lane)
              target-speed (:speed-in this-speed-option)
              vdecision (get-velocity-in-future ticks 5)
              vdecision1 (get-velocity-in-future ticks 6)
              throttle-val (calcs/calculate-throttle-for-tick velocity vdecision vdecision1 (Math/abs (- game-tick @last-turbo-time)) car target-speed)
              next-tick-speed (get-velocity-in-future ticks 1)
              _ (if @in-define-mode (swap! define-data #(conj % car) ))
              
              define-finished  (if (and @in-define-mode (calcs/handle-define @define-data))
                                 (do 
                                   (reset! in-define-mode false)
                                   (future
                                     (reset! current-lanes-a (moz/update-routes moz/track-lanes)))
                                   ))
              ;_ (if (= (rem game-tick 60) 0) (println game-tick))
              ]
          ;(println game-tick ":Velocity: " (format "%.3f" (* 1.0 velocity)) ":v_calc:" 
          ;         (format "%.2f" (* 1. 0 (get-velocity-in-future ticks 0))) ":thro:" throttle-val ":vdec:" vdecision ":vdec1:" vdecision1  ":piece:" (:piece (:location car)) ":piece-dis:" (format "%.2f" (* 1,0 (:piece-distance (:location car)))) ":vtar:" target-speed ":lane-1:" next-lane ":angle:" (:angle car) ":distancein:" (:distance-into-turn this-piece-part) ":remaining:" (:total-turn-length this-piece-part))
          
          ; (println car)
          ;(println game-tick ":Velocity: " (format "%.3f" (* 1.0 velocity)) ":thro:" throttle-val ":vdec:" vdecision ":vdec1:" vdecision1  ":piece:" (:piece (:location car)) ":piece-dis:" (format "%.2f" (* 1,0 (:piece-distance (:location car)))) ":vtar:" target-speed ":lane-1:" next-lane ":angle:" (:angle car) ":distancein:"   (:distance-into-turn this-piece-part) ":remaining:"  (:total-turn-length this-piece-part))
          
          ;(println game-tick ":lane1" this-lane "lane2" next-lane)
          ;(if (> (Math/abs (- velocity @predicted-v)) 0.3)
          ;(println game-tick ":OVERRUN v" velocity "predicted" @predicted-v)
          ;)
          (reset! predicted-v next-tick-speed)
          (cond
            
            @in-define-mode
            
            (if (< game-tick 10)
              {:msgType "throttle" :data 1 :gameTick game-tick}
              {:msgType "throttle" :data 0.8 :gameTick game-tick}
              
              )
            
            (and (> @turbos-available 0) turbos-enabled
                 (> target-speed @rv/turbo-v-a ) 
                 (> (Math/abs (- game-tick @last-turbo-time )) 30))
            
            (do 
              (reset! last-turbo-time game-tick)
              (swap! turbos-available dec)
              (println "++++++Am using turbo!")
              {:msgType "turbo" :data "Yippee!" :gameTick game-tick}
              )
            
            (or
              (not (= @current-throttle throttle-val))
              (> (Math/abs (- game-tick @last-throttle-sent)) 20))
            
            (do
              ;(println game-tick ":Am turning throttle to:" throttle-val)
              (reset! current-throttle throttle-val)
              (reset! last-throttle-sent game-tick)
              {:msgType "throttle" :data throttle-val :gameTick game-tick}
              )
            
            (and (not (= this-lane next-lane))
                 (> (Math/abs (- game-tick @last-switch-sent)) 10)) ;Send Game Switch every 10...
            (do
              ;(println game-tick ":Am Switching Lanes" (get-switch-direction this-lane next-lane))
              (reset! last-switch-sent game-tick)
              {:msgType "switchLane" :data (get-switch-direction this-lane next-lane) :gameTick game-tick}
              )
            
            
            :else
            {:msgType "ping" :data "ping" :gameTick game-tick}
            
            ))))
    (catch Exception e 
           (do
             (.printStackTrace e)
             (println (.getMessage e))
             
             (.printStackTrace e System/out)             
             {:msgType "ping" :data "ping"}
             ))
    ))

  (defmethod handle-msg :default [msg]
    {:msgType "ping" :data "ping"})

  (defn log-msg [msg]
    (case (:msgType msg)
      "join" (println "Joined")
      "gameStart" (println "Race started")
      "crash" (println "Someone crashed")
      "gameEnd" (println "Race ended" (:ticks (:result (first (:results (:data msg))))))
      "error" (println (str "ERROR: " (:results (:data msg))))
      "turboAvailable" (println "turbo received")
      "dnf" (println "-------D N F -----------" msg)
      :noop))


 (defn game-loop [channel]
   (let [msg (read-message channel)]
     ;(println "Message in " (System/currentTimeMillis) (msg/all-game-tick msg))
     (log-msg msg)
     (send-message channel (handle-msg msg))
     (recur channel)))

  (defn manual-connect [host port botname botkey msgType track]
    (let [channel (connect-client-channel host (Integer/parseInt port))]
      (println "manual connect with " track @rv/fmax-a @rv/corner-min-time @rv/corner-min-time @rv/corner-remaining-length @rv/corner-acel-full)
      (reset! in-define-mode true)
      (def log-mode false)
      (def turbos-enabled true)
      
      (send-message channel {:msgType msgType :data {:botId {:name botname :key botkey} :trackName track :carCount 1}})
      (game-loop channel)))


  (defn -main[& [host port botname botkey]]
    (let [channel (connect-client-channel host (Integer/parseInt port))]
      (send-message channel {:msgType "join" :data {:name botname :key botkey}})
      (game-loop channel)))

