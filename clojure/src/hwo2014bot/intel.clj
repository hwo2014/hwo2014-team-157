(ns hwo2014bot.intel
  (:require [hwo2014bot.msg :as msg]
            [hwo2014bot.calcs :as calcs]
            [hwo2014bot.racevariables :as rv]
            [hwo2014bot.models]
            )
  (:import [hwo2014bot.models CarData Location] ))



(def cars (atom {})) ; atom of set containing CarData by key


(defn- update-car [color new-record]
  (swap! cars #(assoc % color new-record)))


(defn get-car [color]
  (get @cars color))

(defn update [msg lanes]
  (doall
    (let [data (:data msg)]
      (map #(
              let [color (-> % :id :color)
                   angle (-> % :angle)
                   piece-index (-> % :piecePosition :pieceIndex)
                   piece-distance (-> % :piecePosition :inPieceDistance)
                   start-lane (-> % :piecePosition :lane :startLaneIndex)
                   end-lane (-> % :piecePosition :lane :endLaneIndex)
                   existing (get @cars color)
                   car-record (if existing existing (CarData. color angle 0 nil 0 nil))
                   new-location (Location. piece-index piece-distance start-lane end-lane)
                   ;get-starting-route lanes (:piece location-1) (:piece-distance location-1)
                ;(:lane1 location-1) (:lane2 location-1)
                   starting-route (calcs/get-starting-route lanes new-location)
                   new-velocity (if existing (calcs/calculate-speed lanes (:location existing) new-location) 0)
                   ]
              (update-car color (assoc car-record :location new-location :velocity new-velocity :starting-route starting-route :previous-angle (:angle car-record) :angle angle))
              ) data)
      )))


(defn- get-car-proximity-list [cars relevant-routes ]

    (map #(list

            (.indexOf relevant-routes (-> % :starting-route :route))
            (-> % :starting-route :piece-parts first :piecepart)
            (* -1 (-> % :starting-route :piece-parts first :length))
            (:color %)
            %
           ) cars)
  )

(defn- third [in]
  (nth in 2)
  )

(defn get-cars-ahead [lanes color routes-ahead]
  (let [this-car (get-car color)
        ;location (:location this-car)
        starting-route (:starting-route this-car)
        route-no (:route starting-route)
        piecepart (first (:piece-parts starting-route))
        piecepart-index (:piecepart piecepart)
        length-remaining (:length piecepart)
        ;test (println "*" route-no piecepart-index length-remaining)
        ;piece-index (:piece this-car)
        ;piece-distance (:piece-distance this-car)
        relevant-routes (for [i (range 0 routes-ahead)]
                          (rem (+ i route-no) (count (first lanes))))
        ;test (println (:starting-route this-car))
        ;test (println (map  #(:velocity %) (vals @cars)))
        proximity-list (get-car-proximity-list (filter #(> (:velocity %) 0.1) (vals @cars))  relevant-routes)
        relevant-proximity-list (filter #(not (neg? (first %)))  proximity-list)
        ;test (println relevant-proximity-list)
        ]

    (take-while #(not (= (:color %) color))
    (reverse (map last (sort-by (juxt first second third) relevant-proximity-list))
        ))))

