(ns hwo2014bot.racevariables)

(def route-version-a (atom 0))
(def decel-multiplier-a (atom 0.02))
(def fmax-a (atom 0.030))
(def track-a (atom []))
(def vmax-a (atom 100.01))
(def vmax-turn-a (atom 16))
(def turbo-v-a (atom 22))
(def aadd-a (atom 0.2))
(def total-pieces (atom nil))

(def calibrated-angle-diff 2.3) ;90% optimal
(def corner-min-time (atom 14))
(def corner-remaining-length (atom 18))
(def corner-acel-full (atom 20))
