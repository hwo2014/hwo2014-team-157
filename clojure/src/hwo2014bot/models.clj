(ns hwo2014bot.models)

(defrecord Location [piece piece-distance lane1 lane2])

(defrecord CarData [color angle previous-angle location velocity starting-route])

(defrecord Route[route lane piece-parts])

(defrecord PiecePart[route piecepart piece lane radius angle length speed-options switch-piece distance-into-turn total-turn-length])

;(defrecord PiecePart[route piecepart piece lane radius angle length speed-options])

(defrecord PieceSpeedOption [lane next-lane speed-in])

(defrecord RacingLine [timing routes ticks])

(defrecord CarRouteCrossing [route lane timing])
