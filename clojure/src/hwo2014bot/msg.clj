(ns hwo2014bot.msg)


(defn multi-all-data [msg color]
  (first (filter #(= (-> % :id :color) color) (:data msg)) ))

(defn all-game-tick [msg]
  (:gameTick msg))

(defn car-position-angle [msg color]
  (:angle (multi-all-data msg color)))

(defn car-position-piece-position [msg color]
  (:piecePosition (multi-all-data msg color)))

(defn car-position-piece-index [msg color]
  (:pieceIndex (car-position-piece-position msg color)))

(defn car-position-lane [msg color]
  (:lane (car-position-piece-position msg color)))

(defn car-position-lap [msg color]
  (:lap (car-position-piece-position msg color)))

(defn car-position-in-piece-distance [msg color]
  (:inPieceDistance (car-position-piece-position msg color)))

(defn car-position-start-lane [msg color]
  (:startLaneIndex (car-position-lane msg color)))

(defn car-position-end-lane [msg color]
  (:endLaneIndex (car-position-lane msg color)))

(defn game-init-track [msg]
  (-> msg :data :race :track))

(defn your-car-color [msg]
  (-> msg :data :color))


