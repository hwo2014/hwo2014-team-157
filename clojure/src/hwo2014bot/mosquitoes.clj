(ns hwo2014bot.mosquitoes
  (:require [hwo2014bot.calcs :as calcs] [hwo2014bot.models :as models] [hwo2014bot.racevariables :as rv])
  (:import [hwo2014bot.models Route PiecePart PieceSpeedOption RacingLine Location] )
  )

(defn filter-options [options valid]
  (let [return-val (filter #(> (.indexOf valid (:next-lane %)) -1) options)]
    return-val))

(defn- get-routes-count [lanes]
  (count (first lanes)))

(defn routes-by-index [lanes route-no]
  (map #(nth % route-no) lanes))

(defn- calc-max-speed [r distance-in length turn-remaining-length]
  
  (let [v-min (+ (* r @rv/fmax-a) 2)
        dist-min (* v-min @rv/corner-min-time)
        dist-remaining-min (* v-min @rv/corner-remaining-length)
        dist-safe (* v-min @rv/corner-acel-full) 
        ]    
  (cond 
    (> distance-in dist-safe)
     @rv/vmax-turn-a
    
    (or (< distance-in dist-min) (> turn-remaining-length dist-remaining-min))
     v-min
    
    :else @rv/vmax-turn-a)))


(defn- get-radius[piece offset]
  (let [r (:radius piece)
        angle (:angle piece)]
    (if r
      (+ r (if (> 0 angle) offset (* -1 offset))))))

(defn- get-length [piece offset]
  (let [r (get-radius piece offset)
        angle (:angle piece)
        length (:length piece)]
    (if (= nil length)
      (* 2 Math/PI r (Math/abs (/ angle 360)))
      length
      )))

(defn- safe-half [val]
  (if val (/ val 2)))


(defn default-piece-speed-options [current-lane lanes-count]
  (for [i (range 0 lanes-count)]
    (PieceSpeedOption. current-lane i 5)
    ))

(defn calc-speed-to-decel [length-in vout]
  (loop [v (max vout 0.1)
         length 0]
    (if (> length length-in)
      v
      (recur (* (/ 1.0 (- 1.0 @rv/decel-multiplier-a)) v) (+ length v))
      )))

(defn calc-speed-out_full_decel [length-in vin number]
  ;(if (> number 5)
   ; (println "Am full decel by > 5 at " number))

  (loop [v vin
         length 0
         v-tick []
         tick 0]
    (if (and (< tick number) (< length length-in))
      (let [
            v-next (- v (* v @rv/decel-multiplier-a))
            ]
        (recur v-next (+ length v-next)  (conj v-tick v-next) (inc tick)))

      (if (> length length-in)
        [v v-tick (+ tick (* -1 (/ (- length length-in) v))) length-in]
        [v v-tick tick length]
        ))))

(defn calc-speed-out_hidden [length-in vin vtarget version] ;TODO Speed improvement here backload!
  (if (or (nil? length-in) (nil? vin) (nil? vtarget) (<= length-in 0))
    (println "calc speed munted" length-in vin vtarget)
    )
  (let [decel (> vin vtarget)]
    (loop [v vin
           length 0
           v-tick []
           tick 0]
      (if (> length length-in)
        [v v-tick
         (+ tick (* -1 (/ (- length length-in) v)))]
        (let [v-accel (if decel 0 @rv/aadd-a)
              v-next-i (- (+ v v-accel) (* v @rv/decel-multiplier-a))
              v-next (if decel (max v-next-i vtarget) (min v-next-i vtarget))
              test (if (> (Math/abs (- v v-next)) 1) (println "Got ya!" v v-next vtarget))
              ]

          (recur v-next (+ length v-next) (conj v-tick v-next) (inc tick))
          )))))
(def calc-speed-out (memoize calc-speed-out_hidden))

(defn zero-pad-speed-out [speed-out min-time]
  ;(println "Am zero padding " speed-out min-time)
  (loop [ticks-out (second speed-out)]
    (if (> (count ticks-out) min-time)
      [0 ticks-out (count ticks-out) ]
      (recur (conj ticks-out 0)))))

(defn join-speed-outs [full-decel ideal]
  [ (first ideal) (into [] (concat (second full-decel) (second ideal))) (+ (nth full-decel 2) (nth ideal 2)) ])

(comment defn calc-speed-out-wrapper [length-in vin vtarget min-time] ;simplify with zero pad..

;  (if (not (or (nil? min-time) (neg? min-time)))
;           (println "min time" min-time)
;          )
  
  (if (or (nil? min-time) (neg? min-time))
    (calc-speed-out length-in vin vtarget @rv/route-version-a)

    (let [first-test (calc-speed-out length-in vin vtarget @rv/route-version-a)]
      (if (> (nth first-test 2) min-time)
        first-test
        (loop [i 1]
          (let [full-decel (calc-speed-out_full_decel length-in vin i)
                length-remaining (- length-in (nth full-decel 3))
                v-remaining (first full-decel)
                ]
            (if (> length-remaining 0)
              (let [ideal (calc-speed-out length-remaining v-remaining vtarget @rv/route-version-a)
                    joint (join-speed-outs full-decel ideal)
                    ]
                (if (> (nth joint 2) min-time)
                  joint
                  (recur (inc i))))
              (zero-pad-speed-out full-decel min-time)))


                )))))

(defn calc-speed-out-wrapper [length-in vin vtarget min-time] 

;  (if (not (or (nil? min-time) (neg? min-time)))
;           (println "min time" min-time)
;          )
  
  (if (or (nil? min-time) (neg? min-time))
    (calc-speed-out length-in vin vtarget @rv/route-version-a)

    (let [first-test (calc-speed-out length-in vin vtarget @rv/route-version-a)]
      (if (> (nth first-test 2) min-time)
        first-test
        (zero-pad-speed-out first-test min-time)))))



(defn calc-route-run [route next-route vin time-in other-races]
  (let [next-lane (:lane next-route)
        piece-parts (:piece-parts route)
        ;_ (println "148" (:route route) other-races) 
        min-time (if (empty? other-races)
                   nil
                   (do ;(println "car in front")
                       (- (:timing (last (sort-by :timing other-races))) time-in)
                       ))]
    (loop [i 0
           total-time 0
           vel vin
           ticks []]
      (if (or (nil? vel))
        (println "calc route run  munted" i total-time vel vin))

      (if (>= i (count piece-parts))
        [total-time vel ticks]
        (let [piece-part (nth piece-parts i)
              options (:speed-options piece-part)
              option (first (filter-options options (list next-lane)))
              speed-out-vec (calc-speed-out-wrapper (:length piece-part) vel (:speed-in option) min-time)
              speed-out (first speed-out-vec)
              speed-out-ticks (second speed-out-vec)
              piece-time (nth speed-out-vec 2)
              ;fail! I want to put in the route here at least and maybe the time?
              new-ticks (into [] (conj ticks [vel speed-out speed-out-ticks (:lane route) next-lane  (:route route)]))]
          (recur
            (inc i)
            (+ total-time piece-time)
            speed-out new-ticks)
          )))))

(defn calc-racing-line-run [racing-line vin other-races]
  ;(println racing-line)
  (let [first-lane (:lane (first racing-line))
        last-lane (:lane (last racing-line))
        second-last-lane (if (> (count racing-line) 1) (:lane (nth racing-line (- (count racing-line) 2))))
        run-calc (loop [i 0
                        total-time 0
                        v vin
                        ticks []]
                   (if (>= i (count racing-line))
                     [total-time v ticks]
                     (let [route (nth racing-line i)
                           next-route (calcs/safe-entry racing-line (inc i))
                           applicable-other-races
                           (filter
                                    #(and
                                      (= (:route %) (:route route))
                                       (= (:lane %) (:lane route))
                                       ) other-races)
                           ;_ (println "197: looking for route,lane" (:route route) (:lane route) "in" other-races)
                           route-val (calc-route-run route next-route v total-time applicable-other-races)
                           ;test (if (> (Math/abs (- v (second route-val))) 2) (println "discrepancy here!" v  (second route-val)))
                           ]
                       (recur (inc i)
                              (+ total-time (first route-val))
                              (second route-val)
                              (concat ticks (nth route-val 2))
                              )
                       )))]

    [(first run-calc) (second run-calc) first-lane last-lane second-last-lane (nth run-calc 2) racing-line]))




(defn filter-invalid-runs [racing-line-calcs]
  (filter #(< (Math/abs (- (nth % 4) (nth % 3) )) 2)  racing-line-calcs))

(defn filter-equivalent-runs [sorted-racing-line-calcs]
  ;(println (first sorted-racing-line-calcs))
  (let [racing-line-calcs (second sorted-racing-line-calcs)
        quickest (first (sort-by (juxt first #(* -1 (second %)))
                                 racing-line-calcs))
        fastest (first (sort-by (juxt #(* -1 (second %)) first ) racing-line-calcs))
        filtered-additional
        (filter #( and (> (first fastest) (first %))
                   (< (second quickest) (second %))
                   ) ;faster than the quickest or quicker than the fastest
                racing-line-calcs)
        filtered (distinct (conj (conj filtered-additional quickest) fastest))
        output-data (map #(str (first %) "," (second %) ","  (first fastest)  "," (second quickest)) filtered)
        ]
    filtered
    ))

(defn filter-redundant-racing-lines [racing-line-calcs]
  (let [possible-runs (filter-invalid-runs racing-line-calcs)
        grouped (group-by #(str (nth % 2) ":" (nth % 3)) possible-runs)
        filtered-groups (map #(filter-equivalent-runs %) grouped)
        return-val (reduce concat filtered-groups)]
    return-val
    ))

(defn calculate-racing-lines [racing-lines vin other-races]
  ;(println "240:" other-races)
  (let [calced-racing-lines (map #(calc-racing-line-run % vin other-races) racing-lines)
        filtered (filter-redundant-racing-lines calced-racing-lines)]
    (pmap #(RacingLine. (first %) (last %) (nth % 5)) filtered)
    ))



(defn add-routes-to-line [existing-lines next-routes]
  (loop [i 0
         return-list []]
    (if (>= i (count existing-lines))
      return-list
      (recur (inc i)
             (let [
                   extended-lines (map #(conj (:routes (nth existing-lines i)) %) next-routes )
                   ]
               (concat return-list extended-lines)
               )))))



(defn update-speeds [lanes starting-racing-lines next-route vin calc-route-depth other-races]
  ;(println "263:" other-races)
  (let [routes-count (get-routes-count lanes)]
    (loop [i 0
           racing-lines starting-racing-lines
           entry next-route]
      (if (>= i calc-route-depth)
        racing-lines
        (let [this-ref (calcs/safe-reference-count routes-count entry)
              pref-ref (calcs/safe-reference-count routes-count (dec entry))
              this-routes (routes-by-index lanes this-ref)
              pref-routes (routes-by-index lanes pref-ref)
              ]

          (if (= nil racing-lines)
            (recur (inc i)
                   (map #(RacingLine. 0 (vector %) []) this-routes)
                   (inc this-ref))
            (recur (inc i)
                   (calculate-racing-lines
                     (add-routes-to-line racing-lines this-routes) vin other-races)
                   (inc this-ref))
            ))))))

;(defn get-racing-line [lanes lane-in lane-out speed-in piece piece-distance next-lane calc-route-depth other-racers]
(defn get-racing-line [lanes speed-in location next-lane calc-route-depth other-racers]

  ;(println "289:" other-racers)
  (first (sort-by first
                  (let [initial-starting-route (calcs/get-starting-route lanes location)
                        starting-route
                        (if (nil? next-lane)
                          (vector initial-starting-route)
                          (let [routes next-lane]
                            (vector initial-starting-route
                                    (nth routes (calcs/safe-reference routes (inc (:route initial-starting-route))))))
                          )

                        ]
                    (if (or (nil? next-lane) (<= (Math/abs (- (:lane initial-starting-route) next-lane)) 1))
                      (update-speeds lanes
                                     (list
                                       (RacingLine. 0 starting-route []))
                                     (inc (:route (last starting-route)))
                                     speed-in
                                     calc-route-depth
                                     other-racers
                                     )
                      nil
                      )))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Route Generation Calcs
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn first-option [route]
  (last
    (sort-by :speed-in (:speed-options (first (:piece-parts route))))))

(defn first-options[lanes route-no]
  (map #(first-option (get % route-no)) lanes))

(defn first-piece [lane route-no]
  ;(println "route-no in" route-no)
   (first (:piece-parts (nth lane route-no)))
  )

(defn update-route[next-options-in route-in next-piece-in]
  (loop [i (dec (count (:piece-parts route-in)))
         route route-in
         next-options next-options-in
         next-piece next-piece-in]
  ;(println "total-turn next piece" (:route next-piece) (:piece next-piece) (:total-turn-length next-piece))

        (if (>= i 0)

      (let [piece-parts (:piece-parts route)
            piece (nth piece-parts i)
            r (:radius piece)
            turn-distance-in (:distance-into-turn piece)
            turn-remaining-length (:total-turn-length piece)
            length (:length piece)
            radius-speed (if r (calc-max-speed r turn-distance-in length turn-remaining-length) @rv/vmax-a)
            speed-options (:speed-options piece)
            new-speed-options

            (map
              #(let [next-option (nth next-options (:next-lane %))
                     next-option-entry (:speed-in next-option)
                     max-speed-next-option (calc-speed-to-decel length next-option-entry)
                     max-speed-in (min @rv/vmax-a radius-speed max-speed-next-option)
                     ]
                 (PieceSpeedOption. (:lane piece) (:next-lane %) max-speed-in)
                 ) speed-options)
            turn-in-val (if (> (:distance-into-turn next-piece) (:distance-into-turn piece))  (+ (:total-turn-length next-piece) (:length piece)) (:length piece))
            next-piece (assoc piece :speed-options
                                    new-speed-options
                                    :total-turn-length turn-in-val)
            ]
        (recur (dec i)
               (assoc route :piece-parts
                      (assoc piece-parts i
                             next-piece))
               new-speed-options
               next-piece))
      route
      )))


(defn update-routes [lanes-in] ;Lane is list of routes
  (swap! rv/route-version-a inc)
  
  (println "****Am updating route speeds etc..." @rv/route-version-a)  
  (let [routes-count (get-routes-count lanes-in)]

    (loop [i 0
           lanes lanes-in
           entry 0]
      (if (> i (* 2 routes-count))
        lanes
        (let [this-ref (calcs/safe-reference-count routes-count entry)
              next-ref (calcs/safe-reference-count routes-count (inc entry))
              next-options (first-options lanes next-ref)
              this-routes (routes-by-index lanes this-ref)
              updated-routes (map #(update-route next-options % (first-piece (nth lanes (:lane %)) next-ref)) this-routes)]
          (recur (inc i)
                 (for [route updated-routes]
                   (assoc (nth lanes (:lane route)) (:route route) route))
                 (dec this-ref)
                 ))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;; Race Init
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defn- clean-first-route[first-pieces last-pieces]
  (into [] (let [all-pieces (concat first-pieces last-pieces)]
             (for [i (range 0 (+ (count all-pieces)))]
               (let [piece (nth all-pieces i)]
                 (assoc piece :route 0 :piecepart i))))))


(defn get-routes [lane lanes-count pieces]
  (let [starting-speed-options (default-piece-speed-options (:index lane) lanes-count)]
    (loop [i 0
           route-count 0
           piece-part-count 0
           routes []
           piece-parts []
           angle-dir 0
           distance-into-turn 0
           ]
      (if (< i (count pieces))
        (let [piece (nth pieces i)
              this-angle (:angle piece)
              safe-angle (if this-angle this-angle 0)
              radius (get-radius piece (:distanceFromCenter lane))
              length (get-length piece (:distanceFromCenter lane))
              this-angle-dir (cond (neg? safe-angle) safe-angle ;Prob not needed anymore, can just use safe-angle!
                                   (pos? safe-angle) safe-angle
                                   :else 0
                                   )
              this-distance-into-turn (if (= this-angle-dir angle-dir)  distance-into-turn 0)
              next-distance-into-turn  (+ this-distance-into-turn length)
              ]
          ;(println i angle-dir this-angle-dir this-distance-into-turn next-distance-into-turn (pos? this-distance-into-turn))

          (if (= (:switch piece) nil)

            (recur (inc i)
                   route-count
                   (inc piece-part-count)
                   routes
                   (conj piece-parts
                         (PiecePart. route-count piece-part-count i (:index lane)
                                     radius
                                     this-angle
                                     length
                                     starting-speed-options
                                     false
                                     this-distance-into-turn
                                     (if (pos? this-distance-into-turn) (+ this-distance-into-turn length) 0)

                                     ))
                   this-angle-dir
                   next-distance-into-turn
                   )

            (recur (inc i)
                   (inc route-count)
                   1
                   (conj routes
                         (Route. route-count
                                 (:index lane)
                                 (conj piece-parts
                                       (PiecePart.
                                         route-count
                                         piece-part-count
                                         i
                                         (:index lane)
                                         radius
                                         (safe-half this-angle)
                                         (safe-half length)
                                         starting-speed-options
                                         true
                                         this-distance-into-turn
                                         (if (pos? this-distance-into-turn) (+ this-distance-into-turn (safe-half length)) 0)

                                         ))))
                   (vector (PiecePart.
                             (inc route-count)
                             0
                             i
                             (:index lane)
                            radius
                             (safe-half (:angle piece))
                             (safe-half length)
                             starting-speed-options
                             true
                             (/ (+ this-distance-into-turn next-distance-into-turn) 2)
                             (if (pos? this-distance-into-turn) (+ this-distance-into-turn (safe-half length)) 0)

                             ))
                   this-angle-dir
                   next-distance-into-turn
                   )))

        (assoc routes 0
               (Route. 0 (:index lane) (clean-first-route piece-parts (:piece-parts (first routes))))
               )))))





(defn init-track [track-data]
  (let [pieces (:pieces track-data)
        lanes-data (:lanes track-data)
        lanes-count (count lanes-data)]

    (do
      (println "Total pieces =" (count (:pieces track-data)))
      (reset! rv/total-pieces (count (:pieces track-data)))
      (def track-lanes (map #(get-routes % lanes-count pieces) lanes-data)))
    ))


