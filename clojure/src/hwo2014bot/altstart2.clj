(ns hwo2014bot.altstart2
  (:require [hwo2014bot.core :as bot]
            [hwo2014bot.racevariables :as rv])
  (:gen-class))


   (defn -main[& [botname botkey msgType type track fmax corner-min corner-remaining corner-full]]

     (if (= type "slow")
       (println (reset! rv/vmax-a 3.012)))

     (reset! rv/fmax-a (read-string fmax))
     (reset! rv/corner-min-time (read-string corner-min))
     (reset! rv/corner-remaining-length (read-string corner-remaining))
     (reset! rv/corner-acel-full (read-string corner-full))

     (bot/manual-connect "testserver.helloworldopen.com" "8091" botname botkey msgType track)
     )
